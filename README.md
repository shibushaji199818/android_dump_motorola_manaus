## user 14 U1TM34.107-17-3 79cd6 release-keys
- Manufacturer: motorola
- Platform: common
- Codename: manaus
- Brand: motorola
- Flavor: user
- Release Version: 14
- Kernel Version: 5.10.198
- Id: U1TM34.107-17-3
- Incremental: 79cd6
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: motorola/manaus_g_hal/manaus:12/U1TM34.107-17-3/79cd6:user/release-keys
- OTA version: 
- Branch: user-14-U1TM34.107-17-3-79cd6-release-keys
- Repo: motorola_manaus_dump
